
package be.johanbas;

import java.util.LinkedList;
import java.util.List;


/**
 * 
 * @author Johan Bas
 */
public class Tak implements Cloneable {

   private String naam;
   private List<Tak> kinderen;
   private boolean isLeaf = false;
   private Tak vader;


   public Tak() {
      kinderen = new LinkedList<Tak>();
   }


   public String getNaam() {
      return naam;
   }


   public void setNaam(String naam) {
      this.naam = naam;
   }


   public List<Tak> getKinderen() {
      return kinderen;
   }


   public void setKinderen(List<Tak> kinderen) {
      this.kinderen = kinderen;
   }


   public boolean isLeaf() {
      return isLeaf;
   }


   public void setLeaf(boolean isLeaf) {
      this.isLeaf = isLeaf;
   }


   public void addKind(Tak kind) {
      kinderen.add(kind);
   }


   public Tak getVader() {
      return vader;
   }


   public void setVader(Tak vader) {
      this.vader = vader;
   }


   public Tak clone(Tak vader) {
      Tak tak = new Tak();
      tak.setNaam(this.getNaam());
      tak.setLeaf(this.isLeaf());
      tak.setVader(vader);
      for (Tak kind : kinderen)
         tak.addKind(kind.clone(tak));

      return tak;
   }


   @Override
   public String toString() {
      String ts = "";

      ts += "Tak [naam=" + naam + ", kinderen=" + kinderen + ", isLeaf=" + isLeaf;

      if (vader != null) // stam heeft vader null
         ts += ", vader=" + vader.getNaam() + " ref:" + vader.hashCode();

      ts += "]";

      return ts;
   }

}
