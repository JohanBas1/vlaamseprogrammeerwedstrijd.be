package be.johanbas;

import java.util.LinkedList;
import java.util.Queue;


/**
 * 
 * @author Johan Bas
 */
public class Hercules {

   public static void snoei(Tak boom) {
      int snoeiTeller = 0;
      
      // bijhouden welke vertakkingen er op de stam zitten
      // hier worden duplicatie van gesnoeide takken ook aan toegevoegd
      // enkel een node uit deze queue verwijderen als het een eindnode is
      Queue<Tak> stamVertakkingen = new LinkedList<>();
      // alle initi�le stamvertakkingen toevoegen
      for (Tak t : boom.getKinderen())
         stamVertakkingen.offer(t);

      // alle stamvertakking per stamvertakking wegsnoeien
      while (!stamVertakkingen.isEmpty()) {
         
         // een eindtak op de stam wordt gesnoeid zonder vermeerdering
         if (stamVertakkingen.peek().isLeaf()) {
            snoeiTeller++;
            stamVertakkingen.poll();
         } 
         
         // stamvertakking is geen eindtak, we gaan op zoek naar een eindtak om te snoeien en te vermeerderen
         else {
            // breadth first search om de dichtsbijzijnde eindvertakking te vinden
            Queue<Tak> bfs = new LinkedList<>();
            for (Tak t : stamVertakkingen.peek().getKinderen())
               bfs.offer(t);

            while (!bfs.isEmpty()) {
               Tak tak = bfs.poll();
               // eindtak? -> snoeien
               if (tak.isLeaf()) {
                  
                  // tak verwijderen uit de boom
                  tak.getVader().getKinderen().remove(tak);
                  
                  // als dit de laatste vertakking van de vader was, wordt de vader leaf
                  if(tak.getVader().getKinderen().size() == 0)
                     tak.getVader().setLeaf(true);
                  
                  // de vermeerdering is op de stam
                  stamVertakkingen.offer(stamVertakkingen.peek().clone(boom));
                  
                  // tellen natuurlijk
                  snoeiTeller++;
                  
                  // opnieuw beginnen vanaf de stam
                  break;
               } else {
                  // geen eindtak, vertakkingen in de queue zetten omdat zij hopelijk eindtak zijn
                  for (Tak vertakking : tak.getKinderen()) {
                     bfs.offer(vertakking);
                  }
               }
            }
         }
      }

      System.out.println(snoeiTeller);
   }
   
}
