
package be.johanbas;

import java.util.List;


/**
 * 
 * @author Johan Bas
 */
public class Hydra {

   public static void main(String[] args) {
      
      List<Tak> bomen = FileIO.bomenInlezen(System.getProperty("user.dir"),"wedstrijd.input");

      for (Tak boom : bomen)
         Hercules.snoei(boom);

   }
}