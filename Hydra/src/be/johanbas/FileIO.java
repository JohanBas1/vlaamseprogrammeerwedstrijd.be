
package be.johanbas;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author Johan Bas
 */
public class FileIO {

   public static List<Tak> bomenInlezen(String pad, String file) {
      Path path = FileSystems.getDefault().getPath(pad, file);
      List<String> lines = null;
      try {
         lines = Files.readAllLines(path, Charset.defaultCharset());
      }
      catch (IOException e) {
         e.printStackTrace();
      }

      // aantal bomen uitlezen en schrappen
      int t_Bomen = Integer.parseInt(lines.get(0));
      lines.remove(0);

      List<Tak> bomen = new LinkedList<>();
      for (int i = 0; i < t_Bomen; i++)
         bomen.add(boomParsen(lines));

      return bomen;
   }


   /**
    * Het parsen van een volledige boom
    * 
    * @param lines
    */
   private static Tak boomParsen(List<String> lines) {
      // aantal vertakkingen van de eerste boom lezen en schrappen
      int n_vertakkingen = Integer.parseInt(lines.get(0));
      lines.remove(0);

      // lijst van alle takken bijhouden om makkelijk de juiste relaties te leggen
      List<Tak> alleTakken = new LinkedList<Tak>();

      // root van de boom aanmaken en uitlezen en schrappen
      Tak stam = new Tak();
      String[] stamString = lines.get(0).split(" ");
      lines.remove(0);
      stam.setNaam(stamString[0]);
      n_vertakkingen--; // want we hebben stam manueel aangemaakt

      // vertakkingen aanmaken voor de stam en alle kinderen aanmaken
      for (int i = 0; i < Integer.parseInt(stamString[1]); i++) {
         Tak t = new Tak();
         t.setVader(stam);

         if (stamString[i + 2].equals("*")) {
            t.setLeaf(true);
         } else {
            t.setNaam(stamString[i + 2]);
         }

         stam.addKind(t);
         alleTakken.add(t);
      }

      // vertakkingen inlezen van de boom waar we aan bezig zijn
      for (int i = 0; i < n_vertakkingen; i++) {
         String[] takString = lines.get(0).split(" ");
         lines.remove(0); // blijven verwijderen zodat de list uiteindelijk klaar staat voor de volgende boom
         

         
         // deze tak moet al aangemaakt zijn???
         for (Tak tak : alleTakken) {
            if (tak.getNaam().equals(takString[0])) {
               for (int j = 2; j < takString.length; j++) {
                  Tak t = new Tak();
                  t.setVader(tak);
                  if (takString[j].equals("*")) {
                     t.setNaam("Leaf");
                     t.setLeaf(true);
                  } else {
                     t.setNaam(takString[j]);
                  }

                  tak.addKind(t);
                  alleTakken.add(t);
               }

               break;
            }
         }
      }

      return stam;
   }

}
