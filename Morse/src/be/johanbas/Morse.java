package be.johanbas;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


/**
 * @author Johan Bas
 */
public class Morse {

   public static void main(String[] args) {

      
      
      Path path = FileSystems.getDefault().getPath(System.getProperty("user.dir"), "wedstrijd.input");
      List<String> lines = null;
      try {
         lines = Files.readAllLines(path, Charset.defaultCharset());
      }
      catch (IOException e) {
         e.printStackTrace();
      }

      
      long time = System.currentTimeMillis();
      
      for (String line : lines)
         morceCoder(line);

      
      System.out.println("Time:"+(System.currentTimeMillis() - time));
   }


   private static void morceCoder(String line) {
      String[] temp = line.split(" ");

      int times = 0;
      StringBuilder morse = new StringBuilder();
      if (temp.length == 1) {
         times = Integer.parseInt(temp[0]);
         morse.append(".");
      } else {
         times = Integer.parseInt(temp[1]);
         morse.append(temp[0]);
      }

      BigInteger punten = BigInteger.ZERO;
      BigInteger strepen = BigInteger.ZERO;
      for (int j = 0; j < morse.length(); j++) {
         if (morse.charAt(j) == '.') {
            punten = punten.add(BigInteger.ONE);
         } else {
            strepen = strepen.add(BigInteger.ONE);
         }
      }

      for (int i = 0; i < times; i++) {
         BigInteger newPunten = BigInteger.ZERO;
         BigInteger newStrepen = BigInteger.ZERO;

         // . -> .-
         // - -> ...-
         newPunten = punten.add(strepen.multiply(new BigInteger("3")));
         newStrepen = punten.add(strepen);

         punten = newPunten;
         strepen = newStrepen;
      }

      System.out.println(strepen.add(punten));
   }
}
